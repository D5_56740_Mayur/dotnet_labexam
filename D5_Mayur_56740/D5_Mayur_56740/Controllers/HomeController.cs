﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using D5_Mayur_56740.Models;

namespace D5_Mayur_56740.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        TestDBEntities dbObject = new TestDBEntities();
        public ActionResult Index()
        {
            var personNameList = dbObject.RationCardTbs.ToList();

            RationCardTb obj = new RationCardTb();
            foreach (var PersonName in personNameList)
            {
                if (PersonName.Role == "admin")
                {
                    obj = PersonName;
                }
            }
            personNameList.Remove(obj);
            dbObject.SaveChanges();

            return View("Index", personNameList.ToList());
        }

        public ActionResult Create()
        {
            return View("Create");
        }

        public ActionResult AfterCreate(RationCardTb employeeToBeAdded)
        {
            dbObject.RationCardTbs.Add(employeeToBeAdded);
            dbObject.SaveChanges();
            return Redirect("/Home/Index");
        }

        public ActionResult Change(string PersonName)
        {
            RationCardTb applicantToUpdated = dbObject.RationCardTbs.Find(PersonName);
            applicantToUpdated.Status = "Approved";

            dbObject.SaveChanges();
            return Redirect("/Home/Index");
        }

       
    }
}