﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using D5_Mayur_56740.Models;

namespace D5_Mayur_56740.Controllers
{
    public class LoginController : Controller
    {
        TestDBEntities dbObject = new TestDBEntities();
        public ActionResult SignIn()
        {
            return View("SignIn");
        }

        public ActionResult AfterSignin(DemoUser user, string ReturnUrl)
        {

            if (check(user))
            {
                FormsAuthentication.SetAuthCookie(user.PersonName, false);
                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index"); //its like going home.. any home url will do..u decide
                }

            }
            else
            {
                ViewBag.Message = "UserName / password is incorrect!";
                return View("SignIn");
            }


        }
        public Boolean check(DemoUser user)
        {
            string role1 = "admin";
            List<RationCardTb> ad = dbObject.RationCardTbs.ToList();

            foreach (RationCardTb u in ad)
            {
                if (u.Role != user.Role)
                {
                    return false;

                }

                else if (u.PersonName == user.PersonName && u.Role == user.Role && u.Role.Equals(role1))
                {
                    return true;
                }
            }
            return false;

        }
    }
}