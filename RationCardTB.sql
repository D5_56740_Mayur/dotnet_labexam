use TestDB;

create table RationCardTb(RationNo int primary key identity(1,1),PersonName varchar(50), Age int, Address varchar(50),
ContactNumber varchar(60),Status varchar(60),Role varchar(60));

insert into RationCardTb values('Mayur',24,'Khandala','12345','Processing','admin');
insert into RationCardTb values('Pk',24,'Khandala','2345','Processing','applicant');
insert into RationCardTb values('Komal',24,'Khandala','2342','Processing','applicant');
insert into RationCardTb values('Hrushi',24,'Khandala','1424','Processing','applicant');
insert into RationCardTb values('Priyanka',24,'Khandala','6789','Processing','applicant');
select*from RationCardTb;



Create table called ‘RationCardTb’ having columns RationNo, PersonName, Age, Address, ContactNumber, Status, Role. 
Enter dummy records into table, one record as admin and four records as applicant(Enter initial Status as Processing).